import System.IO
import qualified Data.Map as M

import XMonad
import XMonad.Util.EZConfig
import XMonad.Config.Xfce
import qualified XMonad.StackSet as W
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog,  doFullFloat, doCenterFloat) 
import XMonad.Hooks.ICCCMFocus
import XMonad.Hooks.SetWMName
import XMonad.Util.WindowProperties (getProp32s)
import XMonad.Actions.UpdatePointer

myModMask = mod4Mask

main = xmonad $ xfceConfig
       { modMask = myModMask
       , manageHook = ( isFullscreen --> doFullFloat ) <+> manageHook xfceConfig <+> myManageHook
       , logHook = takeTopFocus >> setWMName "LG3D" <+> updatePointer ( Relative 0.95 0.95 )
       } 
       `additionalKeys` 
       [ ((myModMask, xK_Left), sendMessage Shrink)
       , ((myModMask, xK_Right), sendMessage Expand)
       ]

myManageHook = composeAll . concat $
               [ [ className =? c --> doFloat | c <- myFloats ]
               , [ title =? t --> doFloat | t <- myOtherFloats ]
               , [ className =? c --> doF (W.shift "2") | c <- chatApps ]
               , [ className =? c --> doF (W.shift "3") | c <- mailApps ]
               ]
    where myFloats = ["VLC", "Gimp", "Plasma", "pidgin"]
          myOtherFloats = ["alsamixer"]
          chatApps = ["Firefox-bin", "Ksirc", "Kopete"]
          mailApps = ["Kontact"]

