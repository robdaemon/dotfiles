import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)

import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog, doFullFloat, doCenterFloat)

-- Make Java apps work
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ICCCMFocus
import XMonad.Hooks.SetWMName
import XMonad.Actions.UpdatePointer

import System.IO

myModMask = mod4Mask

myManageHook = composeAll
               [ className =? "Gimp" --> doFloat
               , className =? "Pidgin" --> doFloat
               ]

main = do
  xmproc <- spawnPipe "/usr/bin/xmobar /home/rob/.xmobarrc"
  xmonad $ defaultConfig
       { manageHook = ( isFullscreen --> doFullFloat ) <+> manageDocks <+> myManageHook <+> manageHook defaultConfig
       , layoutHook = avoidStruts  $  layoutHook defaultConfig
       , logHook = takeTopFocus >> dynamicLogWithPP xmobarPP
                   { ppOutput = hPutStrLn xmproc
                   , ppTitle = xmobarColor "green" "" . shorten 50
                   }
       , startupHook = ewmhDesktopsStartup >> setWMName "LG3D"
       , modMask = myModMask
       }
       `additionalKeys`
       [ ((myModMask, xK_Left),  sendMessage Shrink) -- Win-Left: Shrink this window.
       , ((myModMask, xK_Right), sendMessage Expand) -- Win-Right: Expand this window.
       , ((myModMask, xK_l),     spawn "xscreensaver-command -lock") -- Win-L: lock.
       , ((mod1Mask, xK_Print),  spawn "scrot -u") -- Alt-PrtSc: screenshot active window.
       , ((0, xK_Print),         spawn "scrot -m") -- PrtSc: screenshot everything.
       ]