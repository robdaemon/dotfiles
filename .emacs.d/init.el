;;; .emacs --- My initialization stuff.

;;; Commentary:

;;; Code:

(add-to-list 'load-path "~/.emacs.d/vendor/")

(package-initialize)

(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))

(setq-default indent-tabs-mode nil)

;;(add-to-list 'load-path "/usr/local/lib/erlang/lib/tools-2.6.13/emacs")
;;(setq erlang-root-dir "/usr/local/lib/erlang")
;;(setq exec-path (cons "/usr/local/bin" exec-path))
;;(require 'erlang-start)

(setq default-tab-width 4)

(setq speedbar-mode-hook 
      '(lambda ()
	 (interactive)
	 (other-frame 0)
	 (speedbar-add-supported-extension ".js")
	 (add-to-list 'speedbar-fetch-etags-parse-list
		      '("\\.js" . speedbar-parse-c-or-c++tag))
	 (speedbar-add-supported-extension ".rb")))

(add-hook 'ruby-mode-hook
		  (lambda () (local-set-key "\r" 'newline-and-indent)))

(require 'ruby-electric)
(add-hook 'ruby-mode-hook
		  (lambda () (ruby-electric-mode t)))

;; golang

(let ((gopath (concat (getenv "HOME") "/Source/Go")))
  (setenv "GOPATH" gopath)
  (setenv "PATH" (concat (getenv "PATH")
                         ":/usr/local/go/bin:"
                         gopath "/bin"))
  (setq exec-path (append exec-path
                          `("/usr/local/go/bin"
                            ,(concat gopath "/bin")))))

;;(setenv "GOPATH" "/Users/rob/Source/dryad-schema-service-go")

(require 'go-autocomplete)
(require 'auto-complete-config)

(add-hook 'before-save-hook #'gofmt-before-save)

(add-hook 'go-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c C-r") 'go-remove-unused-imports)))
(add-hook 'go-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c i") 'go-goto-imports)))
;; end golang

(require 'yasnippet)
(yas/global-mode 1)

(require 'magit)

(require 'ruby-block)
(ruby-block-mode t)
(setq ruby-block-highlight-toggle t)
(require 'ruby-end)

(require 'ruby-test-mode)

(add-hook 'ruby-mode-hook
	  '(lambda ()
	     (ruby-end-mode)
	     (ruby-test-mode)))

(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)

(eval-after-load 'ruby-mode
  '(progn
     ;; work around possible elpa bug
     (ignore-errors (require 'ruby-compilation))
     (setq ruby-use-encoding-map nil)
     (add-hook 'ruby-mode-hook 'inf-ruby-keys)
     (define-key ruby-mode-map (kbd "RET") 'reindent-then-newline-and-indent)
     (define-key ruby-mode-map (kbd "C-M-h") 'backward-kill-word)
     (define-key ruby-mode-map (kbd "C-c l") "lambda")))

(global-set-key (kbd "C-h r") 'ri)

(add-to-list 'completion-ignored-extensions ".rbc")

(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Rakefile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . ruby-mode))

;; javascript

(autoload 'js2-mode "js2-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector (vector "#4d4d4c" "#c82829" "#718c00" "#eab700" "#4271ae" "#8959a8" "#3e999f" "#ffffff"))
 '(background-color "#002b36")
 '(background-mode dark)
 '(column-number-mode t)
 '(cursor-color "#839496")
 '(custom-safe-themes (quote ("06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" "fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "4cf3221feff536e2b3385209e9b9dc4c2e0818a69a1cdb4b522756bcdf4e00a4" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default)))
 '(ecb-options-version "2.40")
 '(ecb-source-path (quote ("/home/rob/Sources/hbase_workers" "/home/rob/Sources/simple_daemon_commons" "/home/rob/Sources/simple_daemon" "/home/rob/Sources/report_worker" "/home/rob/Sources/sm_models_java")))
 '(ecb-tip-of-the-day nil)
 '(fci-rule-color "#efefef")
 '(foreground-color "#839496")
 '(global-linum-mode t)
 '(inhibit-startup-screen t)
 '(js2-basic-offset 2)
 '(js2-bounce-indent-p t)
 '(menu-bar-mode nil)
 '(ruby-deep-arglist (quote f))
 '(ruby-deep-indent-paren nil)
 '(scroll-bar-mode (quote right))
 '(show-paren-mode t)
 '(tool-bar-mode nil))

;; end javascript

(require 'autopair)

(defvar autopair-modes '(r-mode ruby-mode))
(defun turn-on-autopair-mode () (autopair-mode 1))
(dolist (mode autopair-modes) (add-hook (intern (concat (symbol-name mode) "-hook")) 'turn-on-autopair-mode))

(require 'paredit)
(defadvice paredit-mode (around disable-autopairs-around (arg))
  "Disable autopairs mode if paredit-mode is turned on"
  ad-do-it
  (if (null ad-return-value)
      (autopair-mode 1)
    (autopair-mode 0)
    ))

(ad-activate 'paredit-mode)

(add-hook 'emacs-lisp-mode-hook       (lambda () (paredit-mode +1)))
(add-hook 'lisp-mode-hook             (lambda () (paredit-mode +1)))
(add-hook 'lisp-interaction-mode-hook (lambda () (paredit-mode +1)))
(add-hook 'scheme-mode-hook           (lambda () (paredit-mode +1)))
(add-hook 'clojure-mode-hook          (lambda () (paredit-mode +1)))

(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))

(require 'nrepl)
(add-hook 'nrepl-interaction-mode-hook 'nrepl-turn-on-eldoc-mode)

(add-hook 'clojure-mode-hook (lambda () (nrepl-interaction-mode +1)))

;; (xterm-mouse-mode)

(load-theme 'sanityinc-tomorrow-night t)

;; (load-theme 'deeper-blue t)

;; (when (display-graphic-p)
;;  (load-theme 'deeper-blue t))

;; (add-to-list 'load-path "~/.emacs.d/vendor/dylan-mode")
;; (autoload 'dylan-mode "dylan-mode" "Dylan-mode" t)
;; (add-to-list 'auto-mode-alist '("\\.dylan\\'" . dylan-mode))

;; ;; (setq inferior-dylan-program "/opt/opendylan-2012.1/bin/dswank") ; your dswank binary
;; (require 'dime)
;; (dime-setup '(dime-dylan dime-repl dime-compiler-notes-tree))

(setq ring-bell-function 'ignore)

(require 'go-autocomplete)
(require 'auto-complete-config)

(add-to-list 'auto-mode-alist '("\\.ddl$" . sql-mode))
(add-hook 'sql-mode-hook
	  (lambda ()
	    (setq indent-tabs-mode nil)
	    (setq tab-width 4)
	    (sql-highlight-ansi-keywords)))

(require 'protobuf-mode)

(defconst my-protobuf-style
  '((c-basic-offset . 4)
	(indent-tabs-mode . nil)))

(add-hook 'protobuf-mode-hook
		  (lambda () (c-add-style "my-style" my-protobuf-style t)))

(add-to-list 'auto-mode-alist '("\\.proto\\'" . protobuf-mode))

(add-hook 'after-init-hook #'global-flycheck-mode)

(put 'upcase-region 'disabled nil)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:slant normal :weight normal :family "Menlo")))))
